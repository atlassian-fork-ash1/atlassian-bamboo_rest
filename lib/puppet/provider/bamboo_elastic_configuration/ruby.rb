require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_provider.rb'))

Puppet::Type.type(:bamboo_elastic_configuration).provide(:ruby, :parent => Puppet::Provider::BambooProvider) do
  desc "Puppet provider to manage Bamboo elastic configuration"

  #REST endpoint to manage audit log
  def self.bamboo_resource
    '/rest/admin/latest/elastic/config/'
  end

  # Map json result from REST endpoint to resource hash
  def self.map_config_to_resource_hash(elastic_config)
    res_hash = {}

    res_hash[:enabled] = elastic_config['enabled'].to_s.intern unless elastic_config['enabled'].nil?
    res_hash[:access_key] = elastic_config['accessKeyId'].to_s unless elastic_config['accessKeyId'].nil?
    res_hash[:secret_key] = elastic_config['secretAccessKey'].to_s unless elastic_config['secretAccessKey'].nil?
    res_hash[:region] = elastic_config['region'].to_s unless elastic_config['region'].nil?
    res_hash[:private_key_file] = elastic_config['privateKeyFile'].to_s unless elastic_config['privateKeyFile'].nil?
    res_hash[:certificate_file] = elastic_config['certificateFile'].to_s unless elastic_config['certificateFile'].nil?
    res_hash[:upload_aws_identifier] = elastic_config['uploadAwsAccountIdentifierToElasticInstances'].to_s.intern unless elastic_config['uploadAwsAccountIdentifierToElasticInstances'].nil?
    res_hash[:max_elastic_instances] = elastic_config['maxNumOfElasticInstances'].to_i unless elastic_config['maxNumOfElasticInstances'].nil?
    res_hash[:allocate_public_ip_to_vpc_instances] = elastic_config['allocatePublicIpToVpcInstances'].to_s.intern unless elastic_config['allocatePublicIpToVpcInstances'].nil?

    # Instance Management Settings
    if !elastic_config['elasticInstanceManagement'].nil?
        elastic_im = elastic_config['elasticInstanceManagement']
        res_hash[:im_type] = elastic_im['type'].to_s unless elastic_im['type'].nil?
        # Only get the im settings if type == Custom
        if elastic_im['type'] == 'Custom'
            res_hash[:im_idle_shutdown_delay_minutes] = elastic_im['idleAgentShutdownDelayMinutes'].to_i unless elastic_im['idleAgentShutdownDelayMinutes'].nil?
            res_hash[:im_allowed_non_bamboo_instances] = elastic_im['allowedNonBambooInstances'].to_i unless elastic_im['allowedNonBambooInstances'].nil?
            res_hash[:im_max_num_to_start] = elastic_im['maxNumOfInstancesStart'].to_i unless elastic_im['maxNumOfInstancesStart'].nil?
            res_hash[:im_queued_build_threshold] = elastic_im['numOfBuildsInQueue'].to_i unless elastic_im['numOfBuildsInQueue'].nil?
            res_hash[:im_elastic_queued_build_threshold] = elastic_im['numOfElasticBuildsInQueue'].to_i unless elastic_im['numOfElasticBuildsInQueue'].nil?
            res_hash[:im_average_queue_time] = elastic_im['avgQueueTimeMinutes'].to_i unless elastic_im['avgQueueTimeMinutes'].nil?
        end
    end

    # Termination Settings
    if !elastic_config['elasticAutoTermination'].nil?
        res_hash[:termination_enabled] = elastic_config['elasticAutoTermination']['enabled'].to_s.intern unless elastic_config['elasticAutoTermination']['enabled'].nil?
        res_hash[:termination_shutdown_delay] = elastic_config['elasticAutoTermination']['shutdownDelay'].to_i unless elastic_config['elasticAutoTermination']['shutdownDelay'].nil?
    end

    res_hash
  end

  # map resource configuration to json that accepted by bamboo REST endpoint
  def map_resource_hash_to_config
    config = {}

    config['enabled'] = resource[:enabled] unless resource[:enabled].nil?
    config['accessKeyId'] = resource[:access_key] unless resource[:access_key].nil?
    config['secretAccessKey'] = resource[:secret_key] unless resource[:secret_key].nil?
    config['region'] = resource[:region] unless resource[:region].nil?
    config['privateKeyFile'] = resource[:private_key_file] unless resource[:private_key_file].nil?
    config['certificateFile'] = resource[:certificate_file] unless resource[:certificate_file].nil?
    config['uploadAwsAccountIdentifierToElasticInstances'] = resource[:upload_aws_identifier] unless resource[:upload_aws_identifier].nil?
    config['maxNumOfElasticInstances'] = resource[:max_elastic_instances] unless resource[:max_elastic_instances].nil?
    config['allocatePublicIpToVpcInstances'] = resource[:allocate_public_ip_to_vpc_instances] unless resource[:allocate_public_ip_to_vpc_instances].nil?

    instance_management = {}
    instance_management['type'] = resource[:im_type] unless resource[:im_type].nil?
    instance_management['idleAgentShutdownDelayMinutes'] = resource[:im_idle_shutdown_delay_minutes] unless resource[:im_idle_shutdown_delay_minutes].nil?
    instance_management['allowedNonBambooInstances'] = resource[:im_allowed_non_bamboo_instances] unless resource[:im_allowed_non_bamboo_instances].nil?
    instance_management['maxNumOfInstancesStart'] = resource[:im_max_num_to_start] unless resource[:im_max_num_to_start].nil?
    instance_management['numOfBuildsInQueue'] = resource[:im_queued_build_threshold] unless resource[:im_queued_build_threshold].nil?
    instance_management['numOfElasticBuildsInQueue'] = resource[:im_elastic_queued_build_threshold] unless resource[:im_elastic_queued_build_threshold].nil?
    instance_management['avgQueueTimeMinutes'] = resource[:im_average_queue_time] unless resource[:im_average_queue_time].nil?
    config['elasticInstanceManagement'] = instance_management unless instance_management.length == 0

    termination_config = {}
    termination_config['enabled'] = resource[:termination_enabled] unless resource[:termination_enabled].nil?
    termination_config['shutdownDelay'] = resource[:termination_shutdown_delay] unless resource[:termination_shutdown_delay].nil?
    config['elasticAutoTermination'] = termination_config unless termination_config.length == 0

    config
  end

  # ask puppet to create the get/set methods automatically
  mk_resource_methods

end
