require 'spec_helper'

describe Puppet::Type.type(:bamboo_security_settings).provider(:ruby) do |variable|
  describe '.map_config_to_resource_hash' do

    let (:bamboo_security_settings) do
      {
        'readOnlyExternalUserManagement'                => true,
        'signUp'                                        => {
          'enabled'        => true,
          'enabledCaptcha' => false
         },
        'displayContactDetailsEnabled'                  => false,
        'restrictedAdministratorRoleEnabled'            => true,
        'bruteForceProtection'                          => {
          'enabled'       => true,
          'loginAttempts' => 60
        },
        'xsrfProtection'                                => {
          'enabled'            => true,
          'disableForHTTPGET'  => true
        },
        'resolveArtifactsContentTypeByExtensionEnabled' => true,
        'soxComplianceModeEnabled'                      => true,
      }
    end

    let (:bamboo_security_settings_partial) do
      {
          'readOnlyExternalUserManagement'                => true,
          'signUp'                                        => {
              'enabled'        => false
          },
          'displayContactDetailsEnabled'                  => false,
          'restrictedAdministratorRoleEnabled'            => true,
          'bruteForceProtection'                          => {
              'enabled'       => false
          },
          'xsrfProtection'                                => {
              'enabled'            => true,
              'disableForHTTPGET'  => true
          },
          'resolveArtifactsContentTypeByExtensionEnabled' => false
      }
    end

    specify "should be able to map REST response correctly" do
      resource_hash = described_class::map_config_to_resource_hash(bamboo_security_settings)
      expect(resource_hash[:read_only_external_user_management]).to eq :true
      expect(resource_hash[:sign_up_enabled]).to eq :true
      expect(resource_hash[:sign_up_enabled_captcha]).to eq :false
      expect(resource_hash[:display_contact_details_enabled]).to eq :false
      expect(resource_hash[:restricted_administrator_role_enabled]).to eq :true
      expect(resource_hash[:brute_force_protection_enabled]).to eq :true
      expect(resource_hash[:brute_force_protection_login_attempts]).to eq 60
      expect(resource_hash[:xsrf_protection_enabled]).to eq :true
      expect(resource_hash[:xsrf_protection_disable_for_http_get]).to eq :true
      expect(resource_hash[:resolve_artifacts_content_type_by_extension_enabled]).to eq :true
      expect(resource_hash[:sox_compliance_mode_enabled]).to eq :true
    end

    specify "should be able to map partial REST response correctly" do
      resource_hash = described_class::map_config_to_resource_hash(bamboo_security_settings_partial)
      expect(resource_hash[:read_only_external_user_management]).to eq :true
      expect(resource_hash[:sign_up_enabled]).to eq :false
      expect(resource_hash[:sign_up_enabled_captcha]).to be nil
      expect(resource_hash[:display_contact_details_enabled]).to eq :false
      expect(resource_hash[:restricted_administrator_role_enabled]).to eq :true
      expect(resource_hash[:brute_force_protection_enabled]).to eq :false
      expect(resource_hash[:brute_force_protection_login_attempts]).to be nil
      expect(resource_hash[:xsrf_protection_enabled]).to eq :true
      expect(resource_hash[:xsrf_protection_disable_for_http_get]).to eq :true
      expect(resource_hash[:resolve_artifacts_content_type_by_extension_enabled]).to eq :false
    end

  end

  describe '#map_resource_to_config' do
    let(:instance) do
      instance = described_class.new()
      resource = {
        :read_only_external_user_management                  => :true,
        :sign_up_enabled                                     => :true,
        :sign_up_enabled_captcha                             => :false,
        :display_contact_details_enabled                     => :false,
        :restricted_administrator_role_enabled               => :false,
        :brute_force_protection_enabled                      => :false,
        :brute_force_protection_login_attempts               => 10,
        :xsrf_protection_enabled                             => :true,
        :xsrf_protection_disable_for_http_get                => :true,
        :resolve_artifacts_content_type_by_extension_enabled => :false,
        :sox_compliance_mode_enabled                         => :false,
      }
      instance.resource = resource
      instance
    end

    let(:bamboo_security_settings) do
      instance.map_resource_hash_to_config
    end

    specify { expect(bamboo_security_settings['readOnlyExternalUserManagement']).to eq :true }
    specify { expect(bamboo_security_settings['signUp']['enabled']).to eq :true }
    specify { expect(bamboo_security_settings['signUp']['enabledCaptcha']).to eq :false }
    specify { expect(bamboo_security_settings['displayContactDetailsEnabled']).to eq :false }
    specify { expect(bamboo_security_settings['restrictedAdministratorRoleEnabled']).to eq :false }
    specify { expect(bamboo_security_settings['bruteForceProtection']['enabled']).to eq :false }
    specify { expect(bamboo_security_settings['bruteForceProtection']['loginAttempts']).to eq 10 }
    specify { expect(bamboo_security_settings['xsrfProtection']['enabled']).to eq :true }
    specify { expect(bamboo_security_settings['xsrfProtection']['disableForHTTPGET']).to eq :true }
    specify { expect(bamboo_security_settings['resolveArtifactsContentTypeByExtensionEnabled']).to eq :false }
    specify { expect(bamboo_security_settings['soxComplianceModeEnabled']).to eq :false }
  end
end