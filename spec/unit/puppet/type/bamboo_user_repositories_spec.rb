require 'spec_helper'

describe Puppet::Type.type(:bamboo_user_repositories) do
  before :each do
    @provider_class = described_class.provide(:simple) do
      mk_resource_methods
      def flush; end
      def self.instances; []; end
    end
    described_class.stubs(:defaultprovider).returns @provider_class
  end

  describe '#name' do
    it 'should only accept value current' do
      expect {described_class.new(:name => 'current', :type => 'Crowd')}.to_not raise_error
      expect {described_class.new(:name => 'not_current', :type => 'Crowd')}.to raise_error(Puppet::ResourceError)
    end
  end
  describe 'type' do
    specify 'Can not be a random string' do
      expect {described_class.new(:name => 'current', :type => "anything")}.to raise_error(Puppet::ResourceError)
    end

    specify 'Can not be nil' do
      expect {described_class.new(:name => 'current', :type => nil)}.to raise_error(Puppet::Error)
    end

    specify 'Can be Crowd' do
      expect {described_class.new(:name => 'current', :type => 'Crowd')}.to_not raise_error
    end

    specify 'Can be Custom' do
      expect {described_class.new(:name => 'current', :type => 'Custom')}.to_not raise_error
    end

    specify 'Can be Local' do
      expect {described_class.new(:name => 'current', :type => 'Local')}.to_not raise_error
    end
  end

  describe 'cache_refresh_interval' do
    specify 'should work withs positive numbers' do
      expect { described_class.new(:name => 'current', :type => 'Crowd', :cache_refresh_interval => 10) }.to_not raise_error
    end

    specify 'should not accept 0' do
      expect { described_class.new(:name => 'any', :type => 'Crowd', :cache_refresh_interval => 0) }.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept negative number' do
      expect { described_class.new(:name => 'any', :type => 'Crowd', :cache_refresh_interval => -1) }.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept non number' do
      expect { described_class.new(:name => 'any', :type => 'Crowd', :cache_refresh_interval => 'test') }.to raise_error(Puppet::ResourceError)
    end

    specify 'Can not be set if type is Custom' do
      expect { described_class.new(:name => 'any', :type => 'Custom', :cache_refresh_interval => 10) }.to raise_error(Puppet::ResourceError)
    end

    specify 'Can not be set if type is Local' do
      expect { described_class.new(:name => 'any', :type => 'Local', :cache_refresh_interval => 10) }.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'application_name' do
    specify 'Work with standard string' do
      expect { described_class.new(:name => 'current', :type => 'Crowd', :application_name => "bamboo") }.to_not raise_error
    end

    specify 'Does not work with empty string' do
      expect { described_class.new(:name => 'any', :type => 'Crowd', :application_name => '') }.to raise_error(Puppet::ResourceError)
    end

    specify 'Can not be set if type is Custom' do
      expect { described_class.new(:name => 'any', :type => 'Custom', :application_name => "bamboo") }.to raise_error(Puppet::ResourceError)
    end

    specify 'Can not be set if type is Local' do
      expect { described_class.new(:name => 'any', :type => 'Local', :application_name => "bamboo") }.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'server_url' do
    specify 'Works with valid url' do
      expect { described_class.new(:name => 'current', :type => 'Crowd', :server_url => 'http://localhost:8095/crowd') }.to_not raise_error
    end

    specify 'Can not be empty' do
      expect { described_class.new(:name => 'current', :type => 'Crowd', :server_url => '') }.to raise_error(Puppet::ResourceError)
    end

    specify 'Can not be set if type is Custom' do
      expect { described_class.new(:name => 'any', :type => 'Custom', :application_name => 'http://localhost:8095/crowd') }.to raise_error(Puppet::ResourceError)
    end

    specify 'Can not be set if type is Local' do
      expect { described_class.new(:name => 'any', :type => 'Local', :application_name => 'http://localhost:8095/crowd') }.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'application_password' do
    specify 'Works with standard string' do
      expect { described_class.new(:name => 'current', :type => 'Crowd', :application_password => 'abc') }.to_not raise_error
    end

    specify 'Can not be empty' do
      expect { described_class.new(:name => 'current', :type => 'Crowd', :application_password => '') }.to raise_error(Puppet::ResourceError)
    end

    specify 'Can not be set if type is Custom' do
      expect { described_class.new(:name => 'any', :type => 'Custom', :application_password => "abc") }.to raise_error(Puppet::ResourceError)
    end

    specify 'Can not be set if type is Local' do
      expect { described_class.new(:name => 'any', :type => 'Local', :application_password => "abc") }.to raise_error(Puppet::ResourceError)
    end
  end

end
