require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_provider.rb'))

Puppet::Type.type(:bamboo_mail_server).provide(:ruby, :parent => Puppet::Provider::BambooProvider) do
  @doc = 'Puppet provider to manage bamboo mail server configuration'

  def self.bamboo_resource
    '/rest/admin/latest/config/mailServer'
  end

  def self.instances
    existing_res = Bamboo::Rest.get_bamboo_settings(self.bamboo_resource) do |value|
      if value.is_a?(Net::HTTPNotFound)
        return []
      elsif
        Bamboo::ExceptionHandler.process(value) { |msg|
          raise "Could not get #{self.bamboo_resource}, #{msg} "
        }
      end
    end
    hash = self.map_config_to_resource_hash(existing_res)
    hash[:name] = 'current'
    [new(hash)]
  end

  def self.map_config_to_resource_hash(bamboo_mail_server)
    config = {
      :from           => bamboo_mail_server['name'],
      :from_address   => bamboo_mail_server['fromAddress'],
      :email_settings => bamboo_mail_server['emailSetting'].to_s.intern
    }
    config[:subject_prefix] = bamboo_mail_server['subjectPrefix'] unless bamboo_mail_server['subjectPrefix'].nil?
    config[:precedence_bulk_header_excluded] = bamboo_mail_server['precedenceBulkHeaderExcluded'].to_s.intern unless bamboo_mail_server['precedenceBulkHeaderExcluded'].nil?
    config[:smtp_server] = bamboo_mail_server['smtpServer'] unless bamboo_mail_server['smtpServer'].nil?
    config[:smtp_port] = bamboo_mail_server['smtpPort'] unless bamboo_mail_server['smtpPort'].nil?
    config[:smtp_username] = bamboo_mail_server['smtpUsername'] unless bamboo_mail_server['smtpUsername'].nil?
    config[:smtp_password] = bamboo_mail_server['smtpPassword'] unless bamboo_mail_server['smtpPassword'].nil?
    config[:tls_enabled] = bamboo_mail_server['tlsEnabled'].to_s.intern unless bamboo_mail_server['tlsEnabled'].nil?
    config[:jndi_location] = bamboo_mail_server['jndiLocation'] unless bamboo_mail_server['jndiLocation'].nil?

    config
  end

  def map_resource_hash_to_config
    config = {}
    if(resource[:from].nil? || resource[:from_address].nil? || resource[:email_settings].nil?)
      raise ArgumentError, 'from, from_address and email_settings cannot be nil'
    end
    config['name'] = resource[:from]
    config['fromAddress'] = resource[:from_address]
    config['subjectPrefix'] = resource[:subject_prefix] unless resource[:subject_prefix].nil?
    config['precedenceBulkHeaderExcluded'] = resource[:precedence_bulk_header_excluded] unless resource[:precedence_bulk_header_excluded].nil?
    config['emailSetting'] = resource[:email_settings]
    config['smtpServer'] = resource[:smtp_server] unless resource[:smtp_server].nil?
    config['smtpPort'] = resource[:smtp_port] unless resource[:smtp_port].nil?
    config['smtpUsername'] = resource[:smtp_username] unless resource[:smtp_username].nil?
    config['smtpPassword'] = resource[:smtp_password] unless resource[:smtp_password].nil?
    config['tlsEnabled'] = resource[:tls_enabled] unless resource[:tls_enabled].nil?
    config['jndiLocation'] = resource[:jndi_location] unless resource[:jndi_location].nil?

    config
  end

  # ask puppet to create the get/set methods automatically
  mk_resource_methods

end
