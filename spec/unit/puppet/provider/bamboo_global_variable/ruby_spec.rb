require 'spec_helper'

provider_class = Puppet::Type.type(:bamboo_global_variable).provider(:ruby)
type_class = Puppet::Type.type(:bamboo_global_variable)

describe provider_class do
  before(:each) do
    allow(Bamboo::Config).to receive(:read_config).and_return(
      {
        :bamboo_base_url => 'http://example.com',
        :admin_username  => 'foobar',
        :admin_password  => 'secret'
      }
    )
  end

  describe '.map_config_to_resource_hash' do
    specify 'should map json correct to resource hashes' do

      json = {
          'globalVariables' => {
            'globalVariables' => [
              {
                'id' => 1,
                'name' => 'foo',
                'value' => 'bar',
                'encrypted' => false,
              },
            ]
          }
        }
      instances = provider_class.map_config_to_resource_hash(json)
      expect (instances[0].name).equal?('foo')
      expect (instances[0].value).equal?('bar')
    end
  end
end
