require 'spec_helper'

describe Puppet::Type.type(:bamboo_agent) do
  describe '#name' do
    specify 'should accept value "local"' do
      expect { described_class.new(:name => "local", :enabled => 'true')}.to_not raise_error
    end

    specify 'should accept value "remote"' do
      expect { described_class.new(:name => "remote", :enabled => 'true')}.to_not raise_error
    end

    specify 'should accept value "elastic"' do
      expect { described_class.new(:name => "elastic", :enabled => 'true')}.to_not raise_error
    end

    specify 'should not accept invalid value' do
      expect { described_class.new(:name => 'mytest', :enabled => 'false')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe '#enabled' do
    specify 'should accept "true"' do
      expect{ described_class.new(:name => 'local', :enabled => 'true')}.to_not raise_error
    end

    specify 'should accept "false"' do
      expect{ described_class.new(:name => 'local', :enabled => 'false')}.to_not raise_error
    end

    specify 'should not accept invalud values' do
      expect{ described_class.new(:name => 'local', :enabled => 'test')}.to raise_error(Puppet::ResourceError)
    end
  end
end
