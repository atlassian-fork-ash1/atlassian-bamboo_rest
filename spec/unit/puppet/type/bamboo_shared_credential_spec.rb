require 'spec_helper'

describe Puppet::Type.type(:bamboo_shared_credential) do
  specify 'ssh_key should not be empty' do
    expect {described_class.new(:name=>'test', :ssh_key=>'')}.to raise_error(Puppet::ResourceError)
  end
end
