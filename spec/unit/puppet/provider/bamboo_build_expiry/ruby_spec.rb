require 'spec_helper'

describe Puppet::Type.type(:bamboo_build_expiry).provider(:ruby) do |variable|
  describe '.map_config_to_resource_hash' do

    let (:bamboo_build_expiry_config) do
      {
        'duration'              => 1,
        'period'                => 'days',
        'buildsToKeep'          => 1,
        'deploymentsToKeep'     => 2,
        'maximumIgnoredLogSize' => -1,
        'expireResults'         => false,
        'expireArtifacts'       => false,
        'expireLogs'            => false,
        'cronExpression'        => '0 0 0 ? * *'
      }
    end

    specify "should be able to map REST response correctly" do
        resource_hash = described_class::map_config_to_resource_hash(bamboo_build_expiry_config)
        expect(resource_hash[:duration]).to eq 1
        expect(resource_hash[:period]).to eq :days
        expect(resource_hash[:builds_to_keep]).to eq 1
        expect(resource_hash[:deployments_to_keep]).to eq 2
        expect(resource_hash[:maximum_ignored_log_size]).to eq -1
        expect(resource_hash[:expire_results]).to eq :false
        expect(resource_hash[:expire_artifacts]).to eq :false
        expect(resource_hash[:expire_logs]).to eq :false
        expect(resource_hash[:cron_expression]).to eq '0 0 0 ? * *'
    end

  end

  describe '#map_resource_to_config' do
    let (:instance) do
      instance = described_class.new()
      resource = {
        :duration                 => 1,
        :period                   => :months,
        :builds_to_keep           => 1,
        :deployments_to_keep      => 2,
        :maximum_ignored_log_size => -1,
        :expire_results           => :false,
        :expire_logs              => :false,
        :expire_artifacts         => :false,
        :cron_expression          => "0 0 0 ? * *"
      }
      instance.resource = resource
      instance
    end

    let(:bamboo_build_expiry_config) do
      instance.map_resource_hash_to_config
    end

    specify { expect(bamboo_build_expiry_config['duration']).to eq 1 }
    specify { expect(bamboo_build_expiry_config['period']).to eq :months }
    specify { expect(bamboo_build_expiry_config['buildsToKeep']).to eq 1 }
    specify { expect(bamboo_build_expiry_config['deploymentsToKeep']).to eq 2 }
    specify { expect(bamboo_build_expiry_config['maximumIgnoredLogSize']).to eq -1 }
    specify { expect(bamboo_build_expiry_config['expireResults']).to eq :false }
    specify { expect(bamboo_build_expiry_config['expireLogs']).to eq :false }
    specify { expect(bamboo_build_expiry_config['expireArtifacts']).to eq :false }
    specify { expect(bamboo_build_expiry_config['cronExpression']).to eq "0 0 0 ? * *" }
  end
end