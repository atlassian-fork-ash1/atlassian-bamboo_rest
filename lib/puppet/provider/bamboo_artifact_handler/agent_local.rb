require 'json'
require File.expand_path(File.join(File.dirname(__FILE__), 'bamboo_artifact_handler_provider.rb'))

Puppet::Type.type(:bamboo_artifact_handler).provide(:agent_local, :parent => Puppet::Provider::BambooArtifactHandlerProvider) do
  desc 'Puppet provider for type agent local artifact handler'

  confine :true => begin
            name.to_s.intern == :agent_local
          end

  def self.bamboo_resource
    '/rest/admin/latest/artifactHandlers/agentLocal'
  end

  def self.artifact_handler_type
    'agent_local'
  end

  def self.map_config_to_resource_hash(artifact_handler_config)
    config = super(artifact_handler_config)
    config[:artifact_storage_location] = artifact_handler_config['attributes']['artifactStorageLocation'] unless artifact_handler_config['attributes'].nil? || artifact_handler_config['attributes']['artifactStorageLocation'].nil?
    config
  end

  def map_resource_hash_to_config
    config = super
    config['attributes']['artifactStorageLocation'] = resource[:artifact_storage_location] unless resource[:artifact_storage_location].nil?
    config
  end

  # ask puppet to create the get/set methods automatically
  mk_resource_methods

end
