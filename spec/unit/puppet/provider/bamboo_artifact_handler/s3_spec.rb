require 'spec_helper'

describe Puppet::Type.type(:bamboo_artifact_handler).provider(:s3) do |variable|
  describe '.map_config_to_resource_hash' do
    specify 'should map bamboo json response to resource hash correctly' do
      json = {
        'sharedArtifactsEnabled'    => true,
        'nonsharedArtifactsEnabled' => true,
        'attributes'                => {
          'accessKeyId'           => "abcd",
          'secretAccessKey'       => 'encrypted',
          'bucketName'            => 'bucketName',
          'region'                => 'US_EAST_1',
          'bucketPath'            => '/data',
          'maxArtifactFileCount'  => 100,
        }
      }
      resource_hash = described_class.map_config_to_resource_hash(json)
      expect(resource_hash[:shared_artifacts_enabled]).to eq :true
      expect(resource_hash[:non_shared_artifacts_enabled]).to eq :true
      expect(resource_hash[:access_key_id]).to eq 'abcd'
      expect(resource_hash[:secret_access_key]).to eq 'encrypted'
      expect(resource_hash[:bucket_name]).to eq 'bucketName'
      expect(resource_hash[:region]).to eq 'us-east-1'
      expect(resource_hash[:bucket_path]).to eq '/data'
      expect(resource_hash[:max_artifact_file_count]).to eq 100
    end
  end

  describe '#map_resource_hash_to_config' do
    specify 'should map resource hash to json correctly' do
      resource = {
        :shared_artifacts_enabled     => :true,
        :non_shared_artifacts_enabled => :true,
        :access_key_id                =>'abcd',
        :secret_access_key            =>'secret',
        :bucket_name                  => 'bucketName',
        :region                       => 'us-east-1',
        :bucket_path                  => '/data',
        :max_artifact_file_count      => 100
      }
      provider = described_class.new
      provider.resource = resource
      json = provider.map_resource_hash_to_config
      expect(json['sharedArtifactsEnabled']).to eq 'true'
      expect(json['nonsharedArtifactsEnabled']).to eq 'true'
      expect(json['attributes']['secretAccessKey']).to eq 'secret'
      expect(json['attributes']['bucketName']).to eq 'bucketName'
      expect(json['attributes']['region']).to eq 'US_EAST_1'
      expect(json['attributes']['bucketPath']).to eq '/data'
      expect(json['attributes']['maxArtifactFileCount']).to eq 100
    end
  end
end
