require 'spec_helper'

describe Puppet::Type.type(:bamboo_security_settings) do
  describe '#name' do
    it 'should only accept value current' do
      expect { described_class.new(:name => 'current', :brute_force_protection_enabled => true, :brute_force_protection_login_attempts => 3)}.to_not raise_error
      expect { described_class.new(:name => 'not_current', :brute_force_protection_enabled => true, :brute_force_protection_login_attempts => 3)}.to raise_error(Puppet::ResourceError)
    end
  end
  describe 'brute_force_protection_login_attempts' do
    specify 'should accept positive integer' do
      expect { described_class.new(:name => 'current', :brute_force_protection_enabled => true, :brute_force_protection_login_attempts => 3)}.to_not raise_error
    end

    specify 'should not accept zero' do
      expect { described_class.new(:name => 'current', :brute_force_protection_enabled => true, :brute_force_protection_login_attempts => 0)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept negative number' do
      expect { described_class.new(:name => 'current', :brute_force_protection_enabled => true, :brute_force_protection_login_attempts => -1)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept non-digital values' do
      expect { described_class.new(:name => 'current', :brute_force_protection_enabled => true, :brute_force_protection_login_attempts => 'abc')}.to raise_error(Puppet::ResourceError)
    end

    specify 'Can not set when brute_force_protection_enabled is nil' do
      expect { described_class.new(:name => 'current', :brute_force_protection_login_attempts => 3)}.to raise_error(Puppet::ResourceError)
    end

    specify 'Can not set when brute_force_protection_enabled is false' do
      expect { described_class.new(:name => 'current', :brute_force_protection_enabled => false, :brute_force_protection_login_attempts => 3)}.to raise_error(Puppet::ResourceError)
    end

    specify 'Can set to nil if brute_force_protection_enabled is false' do
      expect { described_class.new(:name => 'current', :brute_force_protection_enabled => false)}.to_not raise_error
    end
  end

  describe 'sign_up_enabled_captcha' do
    specify 'Can set when sign_up_enabled is true' do
      expect {described_class.new(:name => 'current', :sign_up_enabled => true, :sign_up_enabled_captcha => true)}.to_not raise_error
    end

    specify 'Can set to nil if sign_up_enabled is false' do
      expect {described_class.new(:name => 'current', :sign_up_enabled => false)}.to_not raise_error
    end

    specify 'Can not set when sign_up_enabled is nil' do
      expect { described_class.new(:name => 'current', :sign_up_enabled_captcha => true)}.to raise_error(Puppet::ResourceError)
    end

    specify 'Can not set when sign_up_enabled is false' do
      expect { described_class.new(:name => 'current', :sign_up_enabled => false, :sign_up_enabled_captcha => true)}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'xsrf_protection_disable_for_http_get' do
    specify 'Can set when xsrf_protection_enabled is true' do
      expect {described_class.new(:name => 'current', :xsrf_protection_enabled => true, :xsrf_protection_disable_for_http_get => true)}.to_not raise_error
    end

    specify 'Can set to nil if xsrf_protection_enabled is false' do
      expect {described_class.new(:name => 'current', :xsrf_protection_enabled => false)}.to_not raise_error
    end

    specify 'Can not set when xsrf_protection_enabled is nil' do
      expect { described_class.new(:name => 'current', :xsrf_protection_disable_for_http_get => true)}.to raise_error(Puppet::ResourceError)
    end

    specify 'Can not set when xsrf_protection_enabled is false' do
      expect { described_class.new(:name => 'current', :xsrf_protection_enabled => false, :xsrf_protection_disable_for_http_get => true)}.to raise_error(Puppet::ResourceError)
    end
  end

end
