require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'hash_password.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'util.rb'))

Puppet::Type.newtype(:bamboo_elastic_configuration) do
  @doc = 'Puppet type for bamboo elastic configuration'
  
  newparam(:name, :namevar => true) do
    desc 'Name of the configuration (i.e. current).'

    validate do |value|
      Util.assert_equal_to_current(value)
    end
  end

  newproperty(:enabled) do
    desc 'Whether elastic agents are enabled'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:access_key) do
    desc 'AWS access key'

    validate do |value|
      raise ArgumentError, "access_key must be set" unless (value.is_a? String) and !value.empty?
    end
  end

  newproperty(:secret_key) do
    desc 'AWS secret key'

    # Changes the comparison function
    def insync?(is)
      if(is == :absent and @should[0] != :absent)
        return false
      elsif(is != :absent and @should[0] == :absent)
        return false
      elsif(is == :absent and @should[0] == :absent)
        return true
      else
        Bamboo::HashPassword.check_password_hash(is, @should[0])
      end
    end

    #Log messages are changed so that the password does not appear in the logs.
    def change_to_s(currentvalue, newvalue)
      if currentvalue == :absent
        return "A secrety_key variable has been created"
      else
        return "The secret_key variable has been changed"
      end
    end

    def is_to_s( currentvalue )
      return '[old secret_key variable hash redacted]'
    end

    def should_to_s( newvalue )
      return '[new secret_key variable hash redacted]'
    end

    validate do |value|
      raise ArgumentError, "secret_key must be set" unless (value.is_a? String) and !value.empty?
    end
  end

  newproperty(:region) do
    desc 'AWS region'

    valid_zones = ["US_EAST_1", "US_WEST_1", "US_WEST_2", "EU_WEST_1", "EU_CENTRAL_1", "ASIA_PACIFIC_SE_1",
                   "ASIA_PACIFIC_SE_2", "ASIA_PACIFIC_NE_1", "SOUTH_AMERICA_1", "US_GOV_W1", "CN_NORTH_1"]
    valid_zones.each { |zone| newvalue(zone) }
  end

  newproperty(:private_key_file) do
    desc 'AWS Private Key File'

    validate do |value|
      raise ArgumentError, "private_key_file must be set" unless (value.is_a? String) and !value.empty?
    end
  end

  newproperty(:certificate_file) do
    desc 'Certificate File'

    validate do |value|
      raise ArgumentError, "certificate_file must be set" unless (value.is_a? String) and !value.empty?
    end
  end

  newproperty(:upload_aws_identifier) do
    desc 'Upload AWS identifier'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:max_elastic_instances) do
    desc 'Maximum number of elastic instances'

    validate do |value|
      assertPositive("max_elastic_instances", value)
    end

    munge { |value| Integer(value) }
  end

  newproperty(:allocate_public_ip_to_vpc_instances) do
    desc 'Allocate puplic ips to VPC instances'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:im_type) do
    desc 'Instance Management: Type / Strategy'

    validate do |value|
      raise ArgumentError, "im_type must be specified" unless !value.nil?
      raise ArgumentError, "im_type must be one of Disabled, Custom, Passive, Aggressive or Default" unless
        ["Disabled", "Custom", "Passive", "Aggressive", "Default"].include?(value)
    end
  end

  newproperty(:im_idle_shutdown_delay_minutes) do
    desc 'Instance Management: idle shutdown delay (in minutes)'

    validate do |value|
      if !value.nil?
        assertPositive("im_idle_shutdown_delay_minutes", value)
      end
    end

    munge { |value| Integer(value) }
  end

  newproperty(:im_allowed_non_bamboo_instances) do
    desc 'Instance Management: Number of Non-Bamboo instances allowed'

    validate do |value|
      if !value.nil?
        assertPositive("im_allowed_non_bamboo_instances", value)
      end
    end

    munge { |value| Integer(value) }
  end

  newproperty(:im_max_num_to_start) do
    desc 'Instance Management: Max number of instances to start at once'
    
    validate do |value|
      if !value.nil?
        assertPositive("im_max_num_to_start", value)
      end
    end

    munge { |value| Integer(value) }
  end

  newproperty(:im_queued_build_threshold) do
    desc 'Instance Management: Queued Build Threshold'

    validate do |value|
      if !value.nil?
        assertPositive("im_queued_build_threshold", value)
      end
    end

    munge { |value| Integer(value) }
  end

  newproperty(:im_elastic_queued_build_threshold) do
    desc 'Instance Management: Elastic Queued Build Threshold'

    validate do |value|
      if !value.nil?
        assertPositive("im_elastic_queued_build_threshold", value)
      end
    end

    munge { |value| Integer(value) }
  end

  newproperty(:im_average_queue_time) do
    desc 'Instance Management: Average Queue Time'
    validate do |value|
      if !value.nil?
        assertPositive("im_average_queue_time", value)
      end  
    end

    munge { |value| Integer(value) }
  end

  newproperty(:termination_enabled) do
    desc 'Termination Enabled'

    newvalue(:true)
    newvalue(:false)
  end

  newproperty(:termination_shutdown_delay) do
    desc 'Termination Shutdown Delay'
    validate do |value|
      assertNonNegative("termination_shutdown_delay", value)
    end

    munge { |value| Integer(value) }
  end

  validate do
    # Instance Management config can only be set if im_type is Custom
    raise ArgumentError, "Can only set Instance Management Settings if im_type == \"Custom\"" if
      self[:im_type] != "Custom" and (!self[:im_idle_shutdown_delay_minutes].nil? or
                                     !self[:im_allowed_non_bamboo_instances].nil? or
                                     !self[:im_max_num_to_start].nil? or
                                     !self[:im_queued_build_threshold].nil? or
                                     !self[:im_elastic_queued_build_threshold].nil? or
                                     !self[:im_average_queue_time].nil?)

    raise ArgumentError, "Builds in queue threshold must not be less than elastic builds in queue threshold" if
      !self[:im_queued_build_threshold].nil? and !self[:im_elastic_queued_build_threshold].nil? and
       self[:im_elastic_queued_build_threshold] > self[:im_queued_build_threshold]

    raise ArgumentError, "termination_shutdown_delay can only be set if termination_enabled is true" if
      !self[:termination_shutdown_delay].nil? and self[:termination_enabled] == :false

    raise ArgumentError, "If elastic instance management is disabled, no other properties can be set" if self[:enabled] == :false and
          ( 
            !self[:access_key].nil? or
            !self[:secret_key].nil? or
            !self[:region].nil? or
            !self[:private_key_file].nil? or
            !self[:certificate_file].nil? or
            !self[:upload_aws_identifier].nil? or
            !self[:max_elastic_instances].nil? or
            !self[:allocate_public_ip_to_vpc_instances].nil? or
            !self[:im_type].nil? or
            !self[:termination_enabled].nil? or
            !self[:termination_shutdown_delay].nil?
          )
  end

end

def assertPositive(field, value)
  raise ArgumentError, "#{field} must be a positive integer, got #{value}" unless (value.to_s =~ /\d+/ and value.to_i > 0)
end

def assertNonNegative(field, value)
  raise ArgumentError, "#{field} must be a non-negative integer, got #{value}" unless (value.to_s =~ /\d+/ and value.to_i >= 0)    
end