require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', '..', 'puppet_x', 'bamboo', 'rest.rb'))

Puppet::Type.type(:bamboo_role_permissions).provide(:ruby, :parent => Puppet::Provider) do
  desc 'Puppet provider to manage role permissions in bamboo'

  def self.bamboo_resource
    '/rest/admin/latest/permissions/roles'
  end

  # Since this is the only property of the type, invoke Bamboo REST call in getter method
  def permissions
    rest_permissions = Bamboo::Rest.get_bamboo_settings(self.class.bamboo_resource + "?name=#{resource[:name]}")
    if !rest_permissions.nil? and !rest_permissions.empty?
      return rest_permissions[0]['permissions']
    end
    []
  end

  def permissions=(permissions)
  end

  def map_resource_to_role_permissions_config
    {
      'name'        => resource[:name],
      'permissions' => resource[:permissions],
    }
  end

  def flush
    config = map_resource_to_role_permissions_config
    Bamboo::Rest.update_bamboo_settings(self.class.bamboo_resource, config, 'post')
  end
end
