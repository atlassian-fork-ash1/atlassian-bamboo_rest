require 'openssl'
require 'base64'

module Bamboo
    #A class to decode to compare the stored passwords with the salted and hashed password that is return from the REST endpoints
    class HashPassword
        # The password given from bamboo is salted with a random salt an hashed using PKCS5S2.
        # The result looks like this for the password 'test':
        # {PKCS5S2}PYcs9fI17VC9rfSvOnoI2pzQqaDDtu5t/N97J2Iri6MMs2PcqlVknQRW/kCm4kNx
        # '{PKCS5S2}' Is a prefix that is added to the password. It can be ignored
        # The first 16 characters after that is the the salt
        # The rest of the characters are the salted and hashed password.
        PREFIX_LENGTH = 9
        SALT_LENGTH = 16
        ITERATIONS = 10000
        KEY_LENGTH = 32
        def self.check_password_hash(hashed_password, password)
          if password.nil? || hashed_password.length <= 16
            return false
          end
          #Decode the string withouth the prefix
          raw = Base64.decode64(hashed_password[PREFIX_LENGTH, hashed_password.length])
          #Extract the salt
          salt = raw[0, SALT_LENGTH]
          #Extract the salted and hashed password
          stored_hash = raw[SALT_LENGTH, raw.length]
          #Using the salt given hash the password that is stored in the manifest file
          salted = OpenSSL::PKCS5.pbkdf2_hmac_sha1(password, salt, ITERATIONS, KEY_LENGTH)
          return salted == stored_hash
        end
        # Determine if a variable should be treated as a password variable.
        def self.password_variable?(variable_name)
          variable_name.to_s.downcase =~ /password|secret/
        end
    end
end
