require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_provider.rb'))

Puppet::Type.type(:bamboo_quarantine).provide(:ruby, :parent => Puppet::Provider::BambooProvider) do
  desc "Puppet provider to enable/disable bamboo quarantine tests"

  #REST endpoint to manage quarantine configuration
  def self.bamboo_resource
    '/rest/admin/latest/config/quarantine'
  end

  # Map json result from REST endpoint to resource hash
  def self.map_config_to_resource_hash(quarantine_config)
    {
      :quarantine_tests_enabled => quarantine_config['quarantineTestsEnabled'].to_s.intern,
    }
  end

  # map resource configuration to json that accepted by bamboo REST endpoint
  def map_resource_hash_to_config
    config = {}
    config['quarantineTestsEnabled'] = resource[:quarantine_tests_enabled] unless resource[:quarantine_tests_enabled].nil?

    config
  end

  # ask puppet to create the get/set methods automatically
  mk_resource_methods

end
