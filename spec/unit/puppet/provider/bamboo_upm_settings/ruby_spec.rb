require 'spec_helper'
describe Puppet::Type.type(:bamboo_upm_settings).provider(:ruby) do |variable|
  describe '.map_config_to_resource_hash' do
    let (:bamboo_upm_settings) do
      {
        "settings" => [
          {
            "key" => "pacDisabled",
            "value" => true,
            "requiresRefresh" => true,
            "defaultCheckedValue" => true,
            "readOnly" => false
          },
          {
            "key" => "requestsDisabled",
            "value" => false,
            "requiresRefresh" => true,
            "defaultCheckedValue" => true,
            "readOnly" => false
          },
          {
            "key" => "emailDisabled",
            "value" => true,
            "requiresRefresh" => false,
            "defaultCheckedValue" => true,
            "readOnly" => false
          },
          {
            "key" => "autoUpdateEnabled",
            "value" => false,
            "requiresRefresh" => false,
            "defaultCheckedValue" => false,
            "readOnly" => false
          },
          {
            "key" => "privateListingsEnabled",
            "value" => true,
            "requiresRefresh" => true,
            "defaultCheckedValue" => false,
            "readOnly" => false
          }
        ]
      }
    end

    let(:resource_hash) do
      described_class::map_config_to_resource_hash(bamboo_upm_settings)
    end

    specify { expect(resource_hash[:marketplace_connection_enabled]).to eq :false}
    specify { expect(resource_hash[:request_addon_enabled]).to eq :true}
    specify { expect(resource_hash[:email_notification_enabled]).to eq :false}
    specify { expect(resource_hash[:auto_update_enabled]).to eq :false}
    specify { expect(resource_hash[:private_listings_enabled]).to eq :true}
  end

  describe '#map_resource_hash_to_config' do
    let(:instance) do
      instance = described_class.new()
      resource = {
        :marketplace_connection_enabled  => :false,
        :request_addon_enabled           => :false,
        :email_notification_enabled      => :true,
        :auto_update_enabled             => :false,
        :private_listings_enabled        => :true,
      }
      instance.resource = resource
      instance
    end

    let(:bamboo_upm_settings) do
      instance.map_resource_hash_to_config
    end

    let(:partial_instance) do
      partial_instance = described_class.new()
      resource = {
        :marketplace_connection_enabled  => false
      }
      partial_instance.resource = resource
      partial_instance
    end

    let(:partial_bamboo_upm_settings) do
      partial_instance.map_resource_hash_to_config
    end

    specify { expect(bamboo_upm_settings['settings'][0]['value']).to eq true}
    specify { expect(bamboo_upm_settings['settings'][1]['value']).to eq true}
    specify { expect(bamboo_upm_settings['settings'][2]['value']).to eq false}
    specify { expect(bamboo_upm_settings['settings'][3]['value']).to eq false}
    specify { expect(bamboo_upm_settings['settings'][4]['value']).to eq true}

    specify { expect(partial_bamboo_upm_settings['settings'].size).to eq 1}
    specify { expect(partial_bamboo_upm_settings['settings'][0]['value']).to eq true}
  end
end