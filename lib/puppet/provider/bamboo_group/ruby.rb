require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', '..', 'puppet_x', 'bamboo', 'rest.rb'))

Puppet::Type.type(:bamboo_group).provide(:ruby, :parent => Puppet::Provider) do
  desc 'Puppet provider for bamboo groups'

  # Bamboo REST endpoint to manage groups
  def self.groups_resource
    '/rest/admin/latest/security/groups'
  end

  # Bamboo REST endpoint to manage group permissions
  def self.group_permissions_resource
    '/rest/admin/latest/permissions/groups'
  end

  def initialize(value={})
    super(value)
    @dirty_flag = false
  end

  # Query bamboo for existence of the group, the REST endpoint returns an array.
  def exists?
    groups = Bamboo::Rest.get_bamboo_settings(self.class.groups_resource + "?name=" + resource[:name].to_s)
    !groups.nil? and !groups.empty?
  end

  def map_resource_to_group_permissions_config
    config = {
        'name'     => resource[:name],
    }

    config['permissions'] = resource[:permissions] unless resource[:permissions].nil?
    config
  end

  # Create a group in bamboo and update its permissions if necessary
  def create
    config = { 'name' => resource[:name]}
    Bamboo::Rest.create_bamboo_resource(self.class.groups_resource, config)
    if !resource[:permissions].nil? and !resource[:permissions].empty?
      Bamboo::Rest.update_bamboo_settings(self.class.group_permissions_resource, map_resource_to_group_permissions_config, 'post')
    end
    @dirty_flag = false
  end

  # Bamboo REST endpoint does not support delete method.
  def destroy
    fail("Delete bamboo group is currently not supported")
  end

  def flush
    if @dirty_flag
      config = map_resource_to_group_permissions_config
      Bamboo::Rest.update_bamboo_settings(self.class.group_permissions_resource, config, 'post')
    end
  end

  # Since this is the only property of the type, invoke Bamboo REST call in getter method
  def permissions
    rest_permissions = Bamboo::Rest.get_bamboo_settings(self.class.group_permissions_resource + "?name=#{resource[:name]}")
    if !rest_permissions.nil? and !rest_permissions.empty?
      return rest_permissions[0]['permissions']
    end
    []
  end

  def permissions=(value)
    @dirty_flag = true
  end

end
