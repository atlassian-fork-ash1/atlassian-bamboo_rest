require 'json'
require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_provider.rb'))

class Puppet::Provider::BambooArtifactHandlerProvider < Puppet::Provider::BambooProvider
  desc 'Super class for Bamboo artifact handler providers'

  # Find server local artifact handler configuration via invoking bamboo REST api.
  # Map response from json format to resource properties
  # Invoked during `puppet resource`
  def self.instances
    bamboo_settings = Bamboo::Rest.get_bamboo_settings(self.bamboo_resource, self.content_type)
    hash = map_config_to_resource_hash(bamboo_settings)
    # set resource name to the artifact handler type.
    # When applying the manifest, puppet will choose the right provider based on the resource name
    hash[:name] = self.artifact_handler_type
    [new(hash)]
  end

  def self.map_config_to_resource_hash(artifact_handler_config)
    {
        :shared_artifacts_enabled     => artifact_handler_config['sharedArtifactsEnabled'].to_s.intern,
        :non_shared_artifacts_enabled => artifact_handler_config['nonsharedArtifactsEnabled'].to_s.intern,
    }
  end

  def map_resource_hash_to_config
    config = {}
    config['sharedArtifactsEnabled'] = resource[:shared_artifacts_enabled].to_s unless resource[:shared_artifacts_enabled].nil?
    config['nonsharedArtifactsEnabled'] = resource[:non_shared_artifacts_enabled].to_s unless resource[:non_shared_artifacts_enabled].nil?
    config
  end

end